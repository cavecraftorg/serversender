package top.cavecraft.serversender;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Sorts;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerConnectEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.event.EventHandler;
import org.bson.BsonDocument;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.net.InetSocketAddress;
import java.util.*;

import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Updates.combine;

public final class ServerSender extends Plugin implements Listener {
    private MongoClient mongoClient;
    private MongoCollection<Document> servers;
    private HashMap<UUID, String> playerLocations;

    @Override
    public void onEnable() {
        ConnectionString connectionString = new ConnectionString("mongodb://localhost:27017");
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .retryWrites(true)
                .build();
        mongoClient = MongoClients.create(mongoClientSettings);
        MongoDatabase mongoDatabase = mongoClient.getDatabase("test");
        servers = mongoDatabase.getCollection("servers");
        getProxy().getPluginManager().registerListener(this, this);
    }

    @EventHandler
    public void serverConnect(ServerConnectEvent event) {
        refreshServerList();

        Document document = servers.find(combine(
                eq("gameType", event.getTarget().getName()),
                eq("available", true)
        )).sort(Sorts.descending("players")).first();

        if (document != null) {
            if (playerLocations != null && Objects.equals(playerLocations.get(event.getPlayer().getUniqueId()), document.getString("uuid")) && event.getReason() != ServerConnectEvent.Reason.JOIN_PROXY) {
                event.setCancelled(true);
                return;
            }

            TextComponent sendMessage = new TextComponent("Server: " + event.getTarget().getName() + document.getString("uuid").substring(0, 4));
            sendMessage.setColor(ChatColor.GREEN);
            event.getPlayer().sendMessage(sendMessage);

            String address = document.getString("address");
            int port = document.getInteger("port");
            event.setTarget(getProxy().constructServerInfo(
                    document.getString("uuid"),
                    new InetSocketAddress(address, port),
                    "motd",
                    false
            ));

            if (playerLocations == null) {
                playerLocations = new HashMap<>();
            }
            playerLocations.put(event.getPlayer().getUniqueId(), document.getString("uuid"));
            return;
        }

        event.setCancelled(true);
        TextComponent cancelReason = new TextComponent("There are no available " + event.getTarget().getName() + " servers!");
        cancelReason.setColor(ChatColor.RED);
        if (event.getReason() == ServerConnectEvent.Reason.JOIN_PROXY) {
            event.getPlayer().disconnect(cancelReason);
        } else {
            event.getPlayer().sendMessage(cancelReason);
        }
    }

    public void refreshServerList() {
        getProxy().getServers().clear();

        List<String> gameTypes = new ArrayList<>();

        for (Document document : servers.find()) {
            if (!gameTypes.contains(document.getString("gameType"))) {
                gameTypes.add(document.getString("gameType"));
            }
        }

        for (String gameType : gameTypes) {
            getProxy().getServers().put(gameType, getProxy().constructServerInfo(
                    gameType,
                    null,
                    "motd",
                    false
            ));
        }
    }

    @Override
    public void onDisable() {
        mongoClient.close();
    }
}
